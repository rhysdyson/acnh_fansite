---
layout: page
title: 404 Error - Page Not Found
---

![Resetti](assets/image/resetti.jpg# thumbnail left)

# GRRRAAAGH! WE GOT A RESET!

It looks like SOMEBODY forgot to SAVE!

How many times must I tell you to SAVE before you QUIT. Otherwise, things go wrong. Pages begin to vanish, files can corrupt and the rest of the universe can become unstable. That leaves ME to tunnel new networks to try and fix it.

<a href="javascript:history.back()">
	<button>Time Travel Back</button>
</a>