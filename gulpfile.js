/*-------------------------------
            GULP FILE
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  This gulp file is used to automate various tasks.
  NOTE: SCSS compiling is done through Jekyll.
  
  Task list:
  - Refresh browser on file change/save.
  - Compress raw images, and migrate from _images to img folder.
  - Change css/js file names in html to force a cache reset for end-users.

  Commands:
  - gulp | Run everything you need.
  - img  | Compress and migrate images.
*/

/*-------------------------------
              SETUP
-------------------------------*/

// Dependencies
const { gulp, src, dest, series, parallel } = require('gulp');

const browserSync = require('browser-sync').create();
const watch = require('gulp-watch');
var replace = require('gulp-replace');

const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');

// Variables (file paths)
const path = {
  markup: '**/*.{html,md}',
  styles: '_scss/**/*.{scss,sass,css}',
  imgInput: '_images/**/*.{png,jpg,jpeg,ico}',
  imgOutput: 'assets/image/'
}

/*-------------------------------
            GULP TASKS
-------------------------------*/

exports.default = series(
  imageClean,
  imageCompress,
  cacheTask,
  watchTask
);

exports.img = series(
    imageClean,
    imageCompress
);

// Automatic page reload
function watchTask(){
  browserSync.init({
    server: {
      baseDir: '_site'
    }
  });
  
  watch([
    path.markup,
    path.styles
  ],{delay: 1500, interval: 3000, usePolling: true})
    .on('change', browserSync.reload);
}

// Cache clearing
var cbString = new Date().getTime();

function cacheTask(){
    return src(['_developer/_layouts/**/*.html'])
        .pipe(replace(/stylebust=\d+/, 'stylebust=' + cbString)).pipe(replace(/scriptbust=\d+/, 'scriptbust=' + cbString))
        .pipe(dest('_developer/_layouts/'));
}

// Image compression
function imageClean() {
    return src(path.imgOutput,{read: false,allowEmpty: true})
        .pipe(clean()
    );
}

function imageCompress() {
  return src(path.imgInput)
    .pipe(imagemin())
    .pipe(dest(path.imgOutput)
  );
}